#!/bin/bash

cat system/system/priv-app/PrcMessage/PrcMessage.apk.* 2>/dev/null >> system/system/priv-app/PrcMessage/PrcMessage.apk
rm -f system/system/priv-app/PrcMessage/PrcMessage.apk.* 2>/dev/null
cat system/system/priv-app/ZuiBrowser/ZuiBrowser.apk.* 2>/dev/null >> system/system/priv-app/ZuiBrowser/ZuiBrowser.apk
rm -f system/system/priv-app/ZuiBrowser/ZuiBrowser.apk.* 2>/dev/null
cat system/system/priv-app/ZuiBrowser/lib/arm64/libwebviewchromium.so.* 2>/dev/null >> system/system/priv-app/ZuiBrowser/lib/arm64/libwebviewchromium.so
rm -f system/system/priv-app/ZuiBrowser/lib/arm64/libwebviewchromium.so.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/MotoLiveWallpaper3/MotoLiveWallpaper3.apk.* 2>/dev/null >> system_ext/priv-app/MotoLiveWallpaper3/MotoLiveWallpaper3.apk
rm -f system_ext/priv-app/MotoLiveWallpaper3/MotoLiveWallpaper3.apk.* 2>/dev/null
cat system_ext/priv-app/MotoTaskBar/MotoTaskBar.apk.* 2>/dev/null >> system_ext/priv-app/MotoTaskBar/MotoTaskBar.apk
rm -f system_ext/priv-app/MotoTaskBar/MotoTaskBar.apk.* 2>/dev/null
cat system_ext/priv-app/LeVoiceSTApp/LeVoiceSTApp.apk.* 2>/dev/null >> system_ext/priv-app/LeVoiceSTApp/LeVoiceSTApp.apk
rm -f system_ext/priv-app/LeVoiceSTApp/LeVoiceSTApp.apk.* 2>/dev/null
cat vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null >> vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin
rm -f vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null
cat vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null >> vendor/lib64/libCOSNet_spatial_qnn_quantized.so
rm -f vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null
cat product/preinstall/MotoStylus/MotoStylus.apk.* 2>/dev/null >> product/preinstall/MotoStylus/MotoStylus.apk
rm -f product/preinstall/MotoStylus/MotoStylus.apk.* 2>/dev/null
cat product/preinstall/BaiduSearch/BaiduSearch.apk.* 2>/dev/null >> product/preinstall/BaiduSearch/BaiduSearch.apk
rm -f product/preinstall/BaiduSearch/BaiduSearch.apk.* 2>/dev/null
cat product/preinstall/BaiduSearch/oat/arm/BaiduSearch.vdex.* 2>/dev/null >> product/preinstall/BaiduSearch/oat/arm/BaiduSearch.vdex
rm -f product/preinstall/BaiduSearch/oat/arm/BaiduSearch.vdex.* 2>/dev/null
cat product/preinstall/Douyin/Douyin.apk.* 2>/dev/null >> product/preinstall/Douyin/Douyin.apk
rm -f product/preinstall/Douyin/Douyin.apk.* 2>/dev/null
cat product/preinstall/KuaishouVideo/oat/arm64/KuaishouVideo.vdex.* 2>/dev/null >> product/preinstall/KuaishouVideo/oat/arm64/KuaishouVideo.vdex
rm -f product/preinstall/KuaishouVideo/oat/arm64/KuaishouVideo.vdex.* 2>/dev/null
cat product/preinstall/KuaishouVideo/KuaishouVideo.apk.* 2>/dev/null >> product/preinstall/KuaishouVideo/KuaishouVideo.apk
rm -f product/preinstall/KuaishouVideo/KuaishouVideo.apk.* 2>/dev/null
cat product/preinstall/Weibo/Weibo.apk.* 2>/dev/null >> product/preinstall/Weibo/Weibo.apk
rm -f product/preinstall/Weibo/Weibo.apk.* 2>/dev/null
cat product/preinstall/Weibo/oat/arm/Weibo.vdex.* 2>/dev/null >> product/preinstall/Weibo/oat/arm/Weibo.vdex
rm -f product/preinstall/Weibo/oat/arm/Weibo.vdex.* 2>/dev/null
cat product/preinstall/MojiWeather/oat/arm64/MojiWeather.vdex.* 2>/dev/null >> product/preinstall/MojiWeather/oat/arm64/MojiWeather.vdex
rm -f product/preinstall/MojiWeather/oat/arm64/MojiWeather.vdex.* 2>/dev/null
cat product/preinstall/MojiWeather/MojiWeather.apk.* 2>/dev/null >> product/preinstall/MojiWeather/MojiWeather.apk
rm -f product/preinstall/MojiWeather/MojiWeather.apk.* 2>/dev/null
cat product/preinstall/iBiliPlayer/oat/arm64/iBiliPlayer.vdex.* 2>/dev/null >> product/preinstall/iBiliPlayer/oat/arm64/iBiliPlayer.vdex
rm -f product/preinstall/iBiliPlayer/oat/arm64/iBiliPlayer.vdex.* 2>/dev/null
cat product/preinstall/LenovoScanner/LenovoScanner.apk.* 2>/dev/null >> product/preinstall/LenovoScanner/LenovoScanner.apk
rm -f product/preinstall/LenovoScanner/LenovoScanner.apk.* 2>/dev/null
cat product/preinstall/MotoDesktop/MotoDesktop.apk.* 2>/dev/null >> product/preinstall/MotoDesktop/MotoDesktop.apk
rm -f product/preinstall/MotoDesktop/MotoDesktop.apk.* 2>/dev/null
cat product/preinstall/UCBrowser/UCBrowser.apk.* 2>/dev/null >> product/preinstall/UCBrowser/UCBrowser.apk
rm -f product/preinstall/UCBrowser/UCBrowser.apk.* 2>/dev/null
cat product/preinstall/QQMusic/QQMusic.apk.* 2>/dev/null >> product/preinstall/QQMusic/QQMusic.apk
rm -f product/preinstall/QQMusic/QQMusic.apk.* 2>/dev/null
cat product/preinstall/QQMusic/oat/arm64/QQMusic.vdex.* 2>/dev/null >> product/preinstall/QQMusic/oat/arm64/QQMusic.vdex
rm -f product/preinstall/QQMusic/oat/arm64/QQMusic.vdex.* 2>/dev/null
cat product/preinstall/Amap/Amap.apk.* 2>/dev/null >> product/preinstall/Amap/Amap.apk
rm -f product/preinstall/Amap/Amap.apk.* 2>/dev/null
cat product/preinstall/LenovoClub/LenovoClub.apk.* 2>/dev/null >> product/preinstall/LenovoClub/LenovoClub.apk
rm -f product/preinstall/LenovoClub/LenovoClub.apk.* 2>/dev/null
cat product/preinstall/LenovoSmartHome/LenovoSmartHome.apk.* 2>/dev/null >> product/preinstall/LenovoSmartHome/LenovoSmartHome.apk
rm -f product/preinstall/LenovoSmartHome/LenovoSmartHome.apk.* 2>/dev/null
cat product/preinstall/Health/Health.apk.* 2>/dev/null >> product/preinstall/Health/Health.apk
rm -f product/preinstall/Health/Health.apk.* 2>/dev/null
cat product/preinstall/NewsArticle/NewsArticle.apk.* 2>/dev/null >> product/preinstall/NewsArticle/NewsArticle.apk
rm -f product/preinstall/NewsArticle/NewsArticle.apk.* 2>/dev/null
cat product/preinstall/NewsArticle/oat/arm/NewsArticle.vdex.* 2>/dev/null >> product/preinstall/NewsArticle/oat/arm/NewsArticle.vdex
rm -f product/preinstall/NewsArticle/oat/arm/NewsArticle.vdex.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/MotoIconPackage/MotoIconPackage.apk.* 2>/dev/null >> product/app/MotoIconPackage/MotoIconPackage.apk
rm -f product/app/MotoIconPackage/MotoIconPackage.apk.* 2>/dev/null
